<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Котировки валют</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Котировки валют</h5>
            <div id="currency" class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ "$defCurrency->ISO_Char_Code - $defCurrency->Name" }}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach ($currencies as $c)
                        <button class="dropdown-item" data-id="{{ $c->id }}">
                            {{ "$c->ISO_Char_Code - $c->Name" }}
                        </button>
                    @endforeach
                </div>
            </div>

            <input name="def_currency_id" hidden value="{{ $defCurrency->id }}">

            <form id="quote-form">
                <input name="currency_id" hidden>
                <input name="date" id='datepicker' type='text' class="form-control" value="{{ $today }}"> <br>
                <button id="get-quote" type="button" class="btn btn-primary" onclick="getQuote()">Запросить</button>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table id="quotes-table" class="table">
                <thead>
                <tr>
                    <th scope="col">Дата</th>
                    <th scope="col">Курс</th>
                    <th scope="col">Номинал</th>
                    <th scope="col">Код</th>
                    <th scope="col">Название</th>
                    <th scope="col">Сообщение</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    </body>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</html>
