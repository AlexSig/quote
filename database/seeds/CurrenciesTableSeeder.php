<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = "http://www.cbr.ru/scripts/XML_valFull.asp";

        echo "Connecting to $url...".PHP_EOL;

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);

        $xml = new SimpleXMLElement($response->getBody()->getContents());

        foreach ($xml->Item as $currency)
            Currency::insert([
                'Name' => (string)$currency->Name,
                'ISO_Num_Code' => (int)$currency->ISO_Num_Code,
                'ISO_Char_Code' => (string)$currency->ISO_Char_Code,
            ]);

        echo "Currencies import complete.".PHP_EOL;
    }
}
