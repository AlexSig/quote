<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = ['date'];
    public $timestamps = false;

    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }
}
