<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $timestamps = false;

    public function quotes()
    {
        return $this->hasMany('App\Quote');
    }
}
