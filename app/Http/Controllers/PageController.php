<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index() {
        $currencies = Currency::all()->filter(function ($value) {
            return !empty($value->ISO_Char_Code);
        });

        return view('welcome', [
            'currencies' => $currencies,
            'today' => date("Y-m-d"),
            'defCurrency' => $currencies->firstWhere('id', 15), // usd
        ]);
    }
}
