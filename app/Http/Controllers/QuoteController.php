<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use SimpleXMLElement;

class QuoteController extends Controller
{
    public function get(Request $request)
    {
        if (!$request->filled(['date', 'currency_id']))
            return response()->json([
                'state' => 'error',
                'message' => "Требуются дата и currency_id.",
            ]);

        $currency = Currency::find($request->currency_id);

        if(is_null($currency))
            return response()->json([
                'state' => 'error',
                'message' => "Валюта не найдена",
            ]);

        $quote = $currency->quotes()->firstOrNew(['date' => $request->date]);

        if($quote->exists)
            return response()->json([
                'state' => 'success',
                'message' => "Котировка загружена из БД.",
                'quote' => $quote,
                'currency' => $currency,
            ]);

        // котировки нет в БД
        $url = "http://www.cbr.ru/scripts/XML_daily.asp";

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, [
            'query' => ['date_req' => date("d/m/Y", strtotime($request->date))]
        ]);

        $xml = new SimpleXMLElement($response->getBody()->getContents());

        foreach ($xml->Valute as $valute)
            if ($valute->NumCode == $currency->ISO_Num_Code) {

                $quote->Nominal = (int)$valute->Nominal;
                $quote->Value = (float)str_replace(",", ".", $valute->Value);
                $quote->save();

                return response()->json([
                    'message' => "Котировка загружена с cbr.ru.",
                    'state' => 'success',
                    'quote' => $quote,
                    'currency' => $currency,
                ]);
            }

        return response()->json([
            'state' => 'error',
            'message' => "Котировка не найдена на cbr.ru.",
        ]);
    }
}
