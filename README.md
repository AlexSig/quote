## Котировки валют

Проект сделан на Laravel 6. Посмотреть результат - [http://quote.x-sites.ru/](http://quote.x-sites.ru/)

Требования к серверу:

    PHP >= 7.2.0
    BCMath PHP Extension
    Ctype PHP Extension
    JSON PHP Extension
    Mbstring PHP Extension
    OpenSSL PHP Extension
    PDO PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    MySQL 5.7+
 
 Порядок развертывания:

```sh
$ git clone git@bitbucket.org:AlexSig/quote.git
$ cd quote
$ composer install
$ cp .env.example .env
```

Создаем БД в MySQL и прописываем настройки в .env

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=quote
    DB_USERNAME=homestead
    DB_PASSWORD=secret

Возможно, надо будет использовать DB_HOST=localhost

Затем
```sh
$ php artisan key:generate
$ php artisan migrate:refresh --seed
$ php artisan serve
```

Сайт должен заработать на локальном сервере по адресу [http://127.0.0.1:8000](http://127.0.0.1:8000)

Индексный файл находится в папке `public`.